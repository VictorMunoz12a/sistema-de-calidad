package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={
		"com.example.demo.services.UserService;", "com.example.demo.configs;"})
public class MomentumProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MomentumProjectApplication.class, args);
	}

}

package com.example.demo.configs;

import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

public class WebConfig extends AbstractWebSocketMessageBrokerConfigurer {

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		  registry.addEndpoint("/erp")
          .withSockJS();
}
		
	
	
	 @Override
	   public void configureMessageBroker(MessageBrokerRegistry config) {
	      config.enableSimpleBroker("/topics");
	      config.setApplicationDestinationPrefixes("/app");
	   }

}
